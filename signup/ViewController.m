//
//  ViewController.m
//  signup
//
//  Created by Click Labs133 on 9/17/15.
//  Copyright (c) 2015 dk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UILabel *lblFirstName;
UILabel *lblLastName;
UILabel *lblemail;
UILabel *lblPhoneNum;
UILabel *lblPassword;
UILabel *lblConfirmPassword;
UITextField *txtFirstName;
UITextField *txtLastName;
UITextField *txtEmail;
UITextField *txtPhoneNum;
UITextField *txtPassword;
UITextField *txtConfirmPassword;
UIButton *buttonOK;
UIImageView *image1;
UILabel *heading;
UIImageView *imagetop;




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //imagetop
    
    imagetop =[[UIImageView alloc]init];
    imagetop.frame=CGRectMake(0, 0, 400, 155);
    imagetop.image=[UIImage imageNamed:@"back4.jpeg"];
    [self.view addSubview:imagetop];
    

    
    
    
    
    
    
    
    
    
    
    heading=[[UILabel alloc]init];
    heading.frame=CGRectMake(120, 30, 200, 100);
    heading.textColor=[UIColor blackColor];
    heading.text=@"Sign Up";
    heading.font=[UIFont fontWithName:@"Georgia-BoldItalic" size:34.0f];
    [self.view addSubview:heading];
    
    
    image1 =[[UIImageView alloc]init];
    image1.frame=CGRectMake(0, 155, 400, 600);
    image1.image=[UIImage imageNamed:@"redwallpaper.jpeg"];
    
    // textbox.frame .center=[view11.frame .center ];
    [self.view addSubview:image1];
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    //1
    lblFirstName=[[UILabel alloc]init];
    lblFirstName.frame=CGRectMake(50, 160, 150, 50);
    lblFirstName.text=@"First Name";
    lblFirstName.textColor=[UIColor blackColor];
    [self.view addSubview:lblFirstName];
    
    
    txtFirstName=[[UITextField alloc]init];
    txtFirstName.frame=CGRectMake(180, 160, 180, 42);
    txtFirstName.placeholder=@"first name";
    txtFirstName.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:txtFirstName];
    
    
    
    //2
    
    lblLastName=[[UILabel alloc]init];
    lblLastName.frame=CGRectMake(50, 220, 150, 50);
    lblLastName.text=@"Last Name";
    lblLastName.textColor=[UIColor blackColor];
    [self.view addSubview:lblLastName];
    
    
    txtLastName=[[UITextField alloc]init];
    txtLastName.frame=CGRectMake(180, 220, 180, 42);
    txtLastName.placeholder=@"last name";
    txtLastName.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:txtLastName];
    

    
    
    
    
    
    
    //3
    
    lblemail=[[UILabel alloc]init];
    lblemail.frame=CGRectMake(50, 280, 150, 42);
    lblemail.text=@"email-id";
    lblemail.textColor=[UIColor blackColor];
    [self.view addSubview:lblemail];
    
    
    txtEmail=[[UITextField alloc]init];
    txtEmail.frame=CGRectMake(180, 280, 180, 42);
    txtEmail.placeholder=@"your email id";
    txtEmail.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:txtEmail];
    

    
    
    
    //4
    
    lblPhoneNum=[[UILabel alloc]init];
    lblPhoneNum.frame=CGRectMake(50, 340, 150, 42);
    lblPhoneNum.text=@" Phone Number";
    lblPhoneNum.textColor=[UIColor blackColor];
    [self.view addSubview:lblPhoneNum];
    
    
    txtPhoneNum=[[UITextField alloc]init];
    txtPhoneNum.frame=CGRectMake(180, 340, 180, 42);
    txtPhoneNum.placeholder=@"first name";
    txtPhoneNum.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:txtPhoneNum];
    

    
    
    //5
    
    lblPassword=[[UILabel alloc]init];
    lblPassword.frame=CGRectMake(50, 400, 150, 42);
    lblPassword.text=@"Password";
    lblPassword.textColor=[UIColor blackColor];
    [self.view addSubview:lblPassword];
    
    
    txtPassword=[[UITextField alloc]init];
    txtPassword.frame=CGRectMake(180, 400, 180, 42);
    txtPassword.placeholder=@"enter password";
    txtPassword.backgroundColor=[UIColor lightGrayColor];
    txtPassword.secureTextEntry=YES;
    [self.view addSubview:txtPassword];
    

    
    
    
    //6
    
    lblConfirmPassword=[[UILabel alloc]init];
    lblConfirmPassword.frame=CGRectMake(50, 460, 150, 42);
    lblConfirmPassword.text=@"Re-enter";
    lblConfirmPassword.textColor=[UIColor blackColor];
    [self.view addSubview:lblConfirmPassword];
    
    
    txtConfirmPassword=[[UITextField alloc]init];
    txtConfirmPassword.frame=CGRectMake(180, 460, 180, 42);
    txtConfirmPassword.placeholder=@"Re-enter password";
    txtConfirmPassword.backgroundColor=[UIColor lightGrayColor];
    txtConfirmPassword.secureTextEntry=YES;
    [self.view addSubview:txtConfirmPassword];
    
    
    
    
    
    //BUTTON
    buttonOK=[[UIButton alloc]init];
    buttonOK.frame=CGRectMake(150, 530, 80, 60);
    [buttonOK setTitle:@"GO!!" forState:UIControlStateNormal];
    buttonOK.backgroundColor=[UIColor blackColor];
    [buttonOK addTarget:self action:@selector(SUBMIT:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonOK];
    

    
    
}
-(BOOL)CheckCharacter:(NSString *)text{
    NSCharacterSet *ch=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnm"]invertedSet];
    if([text rangeOfCharacterFromSet:ch].location==NSNotFound)
    {
    NSLog(@"No Special character");}
        else
        {
            NSLog(@"Special character found");
            return YES;
        }
        return NO;
}
-(BOOL)CheckNumber:(NSString *)text{
    NSCharacterSet *num=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]invertedSet];
    if([text rangeOfCharacterFromSet:num].location==NSNotFound)
    {
        NSLog(@"No character");}
    else
    {
        NSLog(@"character found");
        return YES;
    }
    return NO;
}










-(void)SUBMIT:(UIButton *)sender{
    NSString *txtPass=txtPassword.text;
    NSString *txtConPass=txtConfirmPassword.text;
    if ([self CheckCharacter:txtFirstName.text]==YES)
    {
        [self alert1];
    }

    else if ([self CheckCharacter:txtLastName.text]==YES){
        [self alert2];
    }
    else if ([self CheckNumber:txtPhoneNum.text]==YES)
    {
        [self alertnum];
    }
    else if ([txtFirstName.text length]==0 || [txtLastName.text length]==0 || [txtEmail.text length]==0 || [txtPhoneNum.text length ]==0 || [txtPassword.text length]==0 || [txtConfirmPassword.text length]==0)
    {
        [self alertCheckFields];
    
    }
    else if (![txtPass isEqualToString:txtConPass])
    {
        [self alertCheckPassword];
    }
    else
    {
        [self alertSubmit];
    }
}
-(void)alert1{

    UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"first name" message:@"only characters are allowed" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alert1 show];

}



-(void)alert2{
    
    UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:@"last name" message:@"only characters are allowed" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alert2 show];
    
}
-(void)alertnum{
    UIAlertView *alertnum=[[UIAlertView alloc]initWithTitle:@"phone number" message:@"characters found" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alertnum show];



}
-(void)alertSubmit
{
    UIAlertView *alertSubmit=[[UIAlertView alloc]initWithTitle:@"Congratulations!!" message:@"user successfully registered" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alertSubmit show];
}
    
-(void)alertCheckFields{
 

    
    UIAlertView *alertCheckFields=[[UIAlertView alloc]initWithTitle:@"oops!!" message:@"complete your information " delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alertCheckFields show];
}
-(void)alertCheckPassword{
    
    
    
    UIAlertView *alertCheckPassword=[[UIAlertView alloc]initWithTitle:@"oops!" message:@"not matching " delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
    [alertCheckPassword show];
}









- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end